import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

let apiUrl = 'http://damp-waters-71032.herokuapp.com/api/' ;
let headers = new Headers();
	headers.append('Content-Type', 'application/json');

@Injectable()
export class AuthServiceProvider {

	constructor(public http: Http) {
   //console.log('Hello AuthServiceProvider Provider');
}

	Register(name,email,password,c_password) {
		return new Promise((resolve, reject) => {
			this.http.post(apiUrl+'register', {name,email,password,c_password}, {headers: headers})
			.subscribe(res => {
				resolve(res.json());
			}, (err) => { 
				reject(err.json());
			});
		}); 
	}

	Login(email,password) {
		return new Promise((resolve, reject) => {
			this.http.post(apiUrl+'login', {email,password}, {headers: headers})
			.subscribe(res => {
				resolve(res.json());
			}, (err) => { 
				reject(err.json());
			});
		}); 
	}
}
