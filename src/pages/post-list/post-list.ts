import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ActionSheetController, AlertController } from 'ionic-angular';

import { AddPostPage } from '../add-post/add-post';
import { EditPostPage } from '../edit-post/edit-post';

import { PostServiceProvider } from '../../providers/post-service/post-service';

let loading;
@IonicPage()
@Component({
  selector: 'page-post-list',
  templateUrl: 'post-list.html',
})
export class PostListPage {

 	posts:any;
	data:any;
	id:any;
	hasPost=false;
	Length;
	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		public loadingCtrl:LoadingController,
		public postService:PostServiceProvider,
		public alertCtrl:AlertController,
		public actionSheetCtrl:ActionSheetController) {

			this.id = this.navParams.get('id');
	}

	ionViewDidEnter() {
		this.FetchPosts();
	}

	ionViewDidLoad() {
		loading = this.loadingCtrl.create({
			content: 'Fetching posts . . .'
		});

		loading.present();
		this.FetchPosts();
	}


	FetchPosts()
	{
		this.postService.GetPosts(this.id).then((result)=>{
			this.data= result;
			if(this.data)
			{
				if(this.data.success==true)
				{

				this.posts=this.data.data;
				this.Length = this.posts.length;
					if(loading)
						loading.dismiss();
				}
			}
				
				
				
			},(err)=>{
				if(loading)
					loading.dismiss();
				console.log(err);
			});
	}

	AddPost()
	{
		this.navCtrl.push(AddPostPage, {id:this.id});
	}

	showActionSheet(post)
	{
		const actionSheet = this.actionSheetCtrl.create({
			title: post.title,
			buttons: [
			{
				text: 'Edit Post',
				handler: () => {
					this.navCtrl.push(EditPostPage, {post:post, id:this.id});
				}
			},{
				text: 'Delete Post',
				handler: () => {

					let alert = this.alertCtrl.create({
						title: 'Delete Post',
						message: 'Are you sure you want to delete this post?',
						cssClass: 'alertCustomCss',
						buttons: 
						[

						{
							text: 'Yes',
							handler: () => 
							{
								loading = this.loadingCtrl.create({
									content: 'Deleting post . . .'
								});

								loading.present();
								this.postService.DeletePost(post.id).then((result)=>{
									this.data= result;
									if(this.data)
									{
										if(this.data.success ==true)
										{
											loading.dismiss();
											this.FetchPosts();
										}
									}
										
										
								},(err)=>{
									//Display an alert
									console.log(err);
								});
							}

						},
						{
							text: 'No',
							role: 'no',
							handler: () => {
					         // console.log('Cancel clicked');

						     }
						 }
						 ]
						});
					alert.present();


				}
			},{
				text: 'Cancel',
				role: 'cancel',
				handler: () => {
					console.log('Cancel clicked');
				}
			}
			]
		});
		actionSheet.present();
	}

}
