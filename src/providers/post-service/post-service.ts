import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Http, Headers } from '@angular/http';

import { AuthServiceProvider } from '../auth-service/auth-service';

let apiUrl = 'http://damp-waters-71032.herokuapp.com/api/' ;
let headers = new Headers();
	headers.append('Content-Type', 'application/json');
@Injectable()
export class PostServiceProvider {

  constructor(public http: Http, public authService:AuthServiceProvider) {
    
		var token = localStorage.getItem('token');
		headers.append('Authorization', 'Bearer ' + token);
  }

  GetPosts(id)
  {


		return new Promise((resolve, reject) => {
			this.http.get(apiUrl+'posts?categoryId='+ id, {headers: headers})
			.subscribe(res => {
				resolve(res.json());
			}, (err) => { 
				reject(err.json());
			});
		}); 
	}


	AddPost(category_id, user_id, title,body)
	{

		return new Promise((resolve, reject) => {
			this.http.post(apiUrl+'posts', {category_id, user_id, title,body},{headers: headers})
			.subscribe(res => {
				resolve(res.json());
			}, (err) => { 
				reject(err.json());
			});
		}); 
	}

	UpdatePost(id,category_id,user_id, title,body)
	{
		return new Promise((resolve, reject) => {
			this.http.put(apiUrl+'posts/'+id, { category_id,user_id, title,body },{headers: headers})
			.subscribe(res => {
				resolve(res.json());
			}, (err) => { 
				reject(err.json());
			});
		}); 
	}

	DeletePost(id)
	{
		return new Promise((resolve, reject) => {
			this.http.delete(apiUrl+'posts/'+id,{headers: headers})
			.subscribe(res => {
				resolve(res.json());
			}, (err) => { 
				reject(err.json());
			});
		}); 
	}

}
