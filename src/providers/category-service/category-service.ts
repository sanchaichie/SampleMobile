import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { AuthServiceProvider } from '../auth-service/auth-service';

let apiUrl = 'http://damp-waters-71032.herokuapp.com/api/' ;
let headers = new Headers();
	headers.append('Content-Type', 'application/json');


@Injectable()
export class CategoryServiceProvider {

  constructor(public http: Http, public authService:AuthServiceProvider) {
    	var token = localStorage.getItem('token');
		headers.append('Authorization', 'Bearer ' + token);

  }

  GetCategories()
  {
		return new Promise((resolve, reject) => {
			this.http.get(apiUrl+'categories', {headers: headers})
			.subscribe(res => {
				resolve(res.json());
			}, (err) => { 
				reject(err.json());
			});
		}); 
	}

	AddCategory(name,description)
	{

		return new Promise((resolve, reject) => {
			this.http.post(apiUrl+'categories', {name, description},{headers: headers})
			.subscribe(res => {
				resolve(res.json());
			}, (err) => { 
				reject(err.json());
			});
		}); 
	}

	UpdateCategory(id,name,description)
	{
		return new Promise((resolve, reject) => {
			this.http.put(apiUrl+'categories/'+id, {name, description},{headers: headers})
			.subscribe(res => {				
				resolve(res.json());
			}, (err) => { 
				reject(err.json());
			});
		}); 
	}

	DeleteCategory(id)
	{
		return new Promise((resolve, reject) => {
			this.http.delete(apiUrl+'categories/'+id,{headers: headers})
			.subscribe(res => {
				resolve(res.json());
			}, (err) => { 
				reject(err.json());
			});
		}); 
	}

}
