import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { HttpModule, } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { RegisterPage } from '../pages/register/register';
import { LoginPage } from '../pages/login/login';
import { CategoryListPage } from '../pages/category-list/category-list';
import { AddCategoryPage } from '../pages/add-category/add-category';
import { EditCategoryPage } from '../pages/edit-category/edit-category';
import { PostListPage } from '../pages/post-list/post-list';
import { AddPostPage } from '../pages/add-post/add-post';
import { EditPostPage } from '../pages/edit-post/edit-post';
import { LandingPage } from '../pages/landing/landing';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { CategoryServiceProvider } from '../providers/category-service/category-service';
import { PostServiceProvider } from '../providers/post-service/post-service';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    RegisterPage,
    LoginPage,
    CategoryListPage,
    AddCategoryPage,
    EditCategoryPage,
    PostListPage,
    AddPostPage,
    EditPostPage,
    LandingPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    RegisterPage,
    LoginPage,
    CategoryListPage,
    AddCategoryPage,
    EditCategoryPage,
    PostListPage,
    AddPostPage,
    EditPostPage,
    LandingPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider,
    CategoryServiceProvider,
    PostServiceProvider
  ]
})
export class AppModule {}
