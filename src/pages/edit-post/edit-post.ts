import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,LoadingController, AlertController } from 'ionic-angular';


import { PostServiceProvider } from '../../providers/post-service/post-service';

let loading:any;

@IonicPage()
@Component({
	selector: 'page-edit-post',
	templateUrl: 'edit-post.html',
})
export class EditPostPage {

	post:any;
	data:any;
	category_id:string;
	userData:string;
	hasError=false;

	constructor(public navCtrl: NavController, 
		public navParams: NavParams,
		public loadingCtrl:LoadingController,
		public alertCtrl:AlertController,
		public postService:PostServiceProvider) {

		this.category_id = this.navParams.get('id');
		this.post= this.navParams.get('post');
		this.userData = localStorage.getItem('userData');
	}

	Update()
	{

		let alert = this.alertCtrl.create({
			title: 'Update Post',
			message: 'Are you sure you want to update this post?',
			cssClass: 'alertCustomCss',
			buttons: 
			[
			
			{
				text: 'Yes',
				handler: () => 
				{
					loading = this.loadingCtrl.create({
						content: 'Updating post . . .'
					});

					loading.present();

					this.postService.UpdatePost(this.post.id, this.category_id,this.userData, this.post.title, this.post.body).then((result)=>{
						this.data= result;
						if(this.data)
						{

							loading.dismiss();
							if(this.data.success==true)
							{
								this.navCtrl.pop();
							}
						}
									
									
						},(err)=>{
							this.hasError =true;
							this.data = err;
							loading.dismiss();
						console.log(err);
					});
				}

			},
			{
				text: 'No',
				role: 'no',
				handler: () => {
			         // console.log('Cancel clicked');

			     }
			 }
			 ]
			});
		alert.present();
	}

	hasProperty(obj, prop)
	{
		if(obj.hasOwnProperty(prop))
			return true;
		else
			return false;
	}


}
