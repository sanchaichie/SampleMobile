import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController} from 'ionic-angular';


import { CategoryServiceProvider } from '../../providers/category-service/category-service';

let loading:any;

@IonicPage()
@Component({
  selector: 'page-add-category',
  templateUrl: 'add-category.html',
})
export class AddCategoryPage {
	name:string;
	description:string;
	data:any;
	hasError=false;
  constructor(public navCtrl: NavController, 
  			  public navParams: NavParams,
  			  public loadingCtrl:LoadingController,
  			  public alertCtrl:AlertController,
  			  public categoryService:CategoryServiceProvider) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad AddCategoryPage');
  }

  Save()
  {

  	let alert = this.alertCtrl.create({
			title: 'Add Category',
			message: 'Are you sure you want to add this category?',
			cssClass: 'alertCustomCss',
			buttons: 
			[
			
				{
					text: 'Yes',
					handler: () => 
					{
						loading = this.loadingCtrl.create({
								content: 'Saving category . . .'
							});

							loading.present();

						  	this.categoryService.AddCategory(this.name, this.description).then((result)=>{
									this.data= result;
									if(this.data)
									{
										if(this.data.success==true)
										{
											
											loading.dismiss();
											this.navCtrl.pop();
										}
										
									}
									
									
									
							},(err)=>{
								this.hasError =true;
								this.data = err;
								loading.dismiss();
								console.log(err);
							});
					}
							
					},
					{
						text: 'No',
						role: 'no',
						handler: () => {
			         // console.log('Cancel clicked');

			    	}
			 	}
			]
		});
		alert.present();
	
  }	

  hasProperty(obj, prop)
  {
  	if(obj.hasOwnProperty(prop))
  		return true;
  	else
  		return false;
  }

}
