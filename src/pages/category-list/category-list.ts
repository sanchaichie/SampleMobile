import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ActionSheetController, AlertController } from 'ionic-angular';

import { AddCategoryPage } from '../add-category/add-category';
import { EditCategoryPage } from '../edit-category/edit-category';
import { PostListPage } from '../post-list/post-list';
import { LoginPage } from '../login/login';

import { CategoryServiceProvider } from '../../providers/category-service/category-service';
let loading:any;
@IonicPage()
@Component({
	selector: 'page-category-list',
	templateUrl: 'category-list.html',
})
export class CategoryListPage {

	categories:any;
	data:any;
	Length;
	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		public loadingCtrl:LoadingController,
		public alertCtrl:AlertController,
		public categoryService:CategoryServiceProvider,
		public actionSheetCtrl:ActionSheetController) {
	}

	ionViewDidEnter() {
		this.FetchCategories();
	}

	ionViewDidLoad() {
		loading = this.loadingCtrl.create({
			content: 'Fetching categories . . .'
		});

		if(!loading)
			loading.present();
		this.FetchCategories();
	}


	FetchCategories()
	{
		this.categoryService.GetCategories().then((result)=>{
			this.data= result;
			if(this.data)
			{
				this.categories=this.data.data;
				this.Length = this.categories.length;
				if(this.data.success==true)
				{
					if(loading)
						loading.dismiss();
				}
			}				
				
			},(err)=>{
				var error= err;
				if(loading)
					loading.dismiss();

				if(error.message =="Unauthenticated.")
				{
					localStorage.clear();
					this.navCtrl.setRoot(LoginPage);
				}
				console.log(err);
			});
	}

	AddCategory()
	{
		this.navCtrl.push(AddCategoryPage);
	}

	showActionSheet(category)
	{
		const actionSheet = this.actionSheetCtrl.create({
			title: category.name,
			buttons: [
			{
				text: 'Edit Category',
				handler: () => {
					this.navCtrl.push(EditCategoryPage, {category:category});
				}
			},{
				text: 'Delete Category',
				handler: () => {

					let alert = this.alertCtrl.create({
						title: 'Delete Category',
						message: 'Are you sure you want to delete this category?',
						cssClass: 'alertCustomCss',
						buttons: 
						[

						{
							text: 'Yes',
							handler: () => 
							{
								loading = this.loadingCtrl.create({
									content: 'Deleting category . . .'
								});

								loading.present();
								this.categoryService.DeleteCategory(category.id).then((result)=>{
									this.data= result;
									if(this.data)
									{
										if(this.data.success ==true)
										{
											loading.dismiss();
											this.FetchCategories();
										}
									}
										
										
										
								},(err)=>{
									//Display an alert
									console.log(err);
								});
							}

						},
						{
							text: 'No',
							role: 'no',
							handler: () => {
					         // console.log('Cancel clicked');

						     }
						 }
						 ]
						});
					alert.present();


				}
			},{
				text: 'Cancel',
				role: 'cancel',
				handler: () => {
					//console.log('Cancel clicked');
				}
			}
			]
		});
		actionSheet.present();
	}

	categorySelected(id)
	{
		this.navCtrl.push(PostListPage,{id:id})
	}

	Logout()
	{
		localStorage.clear();
		this.navCtrl.setRoot(LoginPage);
	}

}
