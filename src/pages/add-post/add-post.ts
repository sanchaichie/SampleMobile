import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController , AlertController} from 'ionic-angular';

import { PostServiceProvider } from '../../providers/post-service/post-service';
let loading;
@IonicPage()
@Component({
  selector: 'page-add-post',
  templateUrl: 'add-post.html',
})
export class AddPostPage {
	title:string;
	body:string;
	category_id:string;
	userData:string;
	data:any;
	hasError=false;
  constructor(public navCtrl: NavController, 
  			  public navParams: NavParams,
  			  public loadingCtrl:LoadingController,
  			  public alertCtrl:AlertController,
  			  public postService:PostServiceProvider) {
  	this.category_id = this.navParams.get('id');
  	this.userData = localStorage.getItem('userData');
  	
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad AddCategoryPage');
  }

  Save()
  {

  	let alert = this.alertCtrl.create({
			title: 'Add Post',
			message: 'Are you sure you want to add this post?',
			cssClass: 'alertCustomCss',
			buttons: 
			[
			
				{
					text: 'Yes',
					handler: () => 
					{
						loading = this.loadingCtrl.create({
								content: 'Saving post . . .'
							});

							loading.present();

						  	this.postService.AddPost(this.category_id,this.userData,this.title, this.body).then((result)=>{
									this.data= result;
									if(this.data)
									{
										if(this.data.success==true)
										{
											
											loading.dismiss();
											this.navCtrl.pop();
										}
									}
									
									
									
							},(err)=>{								
								this.hasError =true;
								this.data = err;
								loading.dismiss();
								//Display an alert
								console.log(err);
							});
					}
							
					},
					{
						text: 'No',
						role: 'no',
						handler: () => {
			         // console.log('Cancel clicked');

			    	}
			 	}
			]
		});
		alert.present();
	
  }

  hasProperty(obj, prop)
  {
  	if(obj.hasOwnProperty(prop))
  		return true;
  	else
  		return false;
  }

}
