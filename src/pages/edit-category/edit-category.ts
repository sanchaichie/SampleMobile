import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,LoadingController , AlertController} from 'ionic-angular';


import { CategoryServiceProvider } from '../../providers/category-service/category-service';

let loading:any;

@IonicPage()
@Component({
  selector: 'page-edit-category',
  templateUrl: 'edit-category.html',
})
export class EditCategoryPage {

	category:any;
	data:any;
	hasError=false;
	
  constructor(public navCtrl: NavController, 
  			  public navParams: NavParams,
  			  public loadingCtrl:LoadingController,
  			  public alertCtrl:AlertController,
  			  public categoryService:CategoryServiceProvider) {

  	this.category= this.navParams.get('category');
  }

  

  Update()
  {

  		let alert = this.alertCtrl.create({
			title: 'Update Category',
			message: 'Are you sure you want to update this category?',
			cssClass: 'alertCustomCss',
			buttons: 
			[
			
				{
					text: 'Yes',
					handler: () => 
					{
						loading = this.loadingCtrl.create({
								content: 'Updating category . . .'
							});

							loading.present();

						  	this.categoryService.UpdateCategory(this.category.id, this.category.name, this.category.description).then((result)=>{
									this.data= result;
									if(this.data)
									{
										if(this.data.success==true)
										{
											
											loading.dismiss();
											this.navCtrl.pop();
										}
									}
									
									
							},(err)=>{
								this.hasError =true;
								this.data = err;
								loading.dismiss();
								console.log(err);
							});
					}
							
					},
					{
						text: 'No',
						role: 'no',
						handler: () => {
			         // console.log('Cancel clicked');

			    	}
			 	}
			]
		});
		alert.present();
  		
  }
   hasProperty(obj, prop)
  {
  	if(obj.hasOwnProperty(prop))
  		return true;
  	else
  		return false;
  }

}
