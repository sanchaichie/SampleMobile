import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

import {AuthServiceProvider } from '../../providers/auth-service/auth-service';

import { CategoryListPage } from '../category-list/category-list';
import { LoginPage } from '../login/login';

let loading:any;
@IonicPage()
@Component({
	selector: 'page-register',
	templateUrl: 'register.html',
})
export class RegisterPage {
	name:string;
	email:string;
	password:string;
	c_password:string;
	data:any;
	hasError=false;
	
	constructor(public navCtrl: NavController, 
					public navParams: NavParams,
					public loadingCtrl:LoadingController,
					public authService:AuthServiceProvider,
					public alertCtrl:AlertController) {
	}

	Register()
	{

		let alert = this.alertCtrl.create({
			title: 'Register Account',
			message: 'Are you sure you want to submit your registration?',
			cssClass: 'alertCustomCss',
			buttons: 
			[
			
				{
					text: 'Yes',
					handler: () => 
					{
						loading = this.loadingCtrl.create({
								content: 'Register Account. . .'
							});

							loading.present();

								this.authService.Register(this.name, this.email,this.password, this.c_password).then((result)=>{
									this.data = result;
									if(this.data)
									{
										loading.dismiss();
										if(this.data.success==true)
										{
											localStorage.setItem('token', this.data.data.token);
											localStorage.setItem('userData', this.data.data.id);
											this.navCtrl.setRoot(CategoryListPage);
										}
										
									}

								},(err)=>{
									this.data=err;
									this.hasError=true;
									loading.dismiss();
									console.log(err);
								});
					}
							
					},
					{
						text: 'No',
						role: 'no',
						handler: () => {
			         // console.log('Cancel clicked');

			    	}
			 	}
			]
		});
		alert.present();

	}

	GoToLogin()
	{
		this.navCtrl.setRoot(LoginPage);
	}

	hasProperty(obj, prop)
	{
		if(obj.hasOwnProperty(prop))
			return true;
		else
			return false;
	}
	

}
