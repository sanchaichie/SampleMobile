import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { LoginPage } from '../login/login';
import { CategoryListPage } from '../category-list/category-list';

@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})
export class LandingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.isLogin();
  }

  isLogin()
  {
   	 setTimeout(()=>{
		if(localStorage.getItem('userData') != null)
	  	{

	 		this.navCtrl.setRoot(CategoryListPage);
	 	}
	 	else
	 	{
	  		this.navCtrl.setRoot(LoginPage);
	 	}
	
   	}, 3000);
  }

}
