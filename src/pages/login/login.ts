import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

import { RegisterPage } from '../register/register';
import { CategoryListPage } from '../category-list/category-list';

import {AuthServiceProvider } from '../../providers/auth-service/auth-service';
let loading:any;
@IonicPage()
@Component({
	selector: 'page-login',
	templateUrl: 'login.html',
})
export class LoginPage {

	email:string;
	password:string;
	data:any;
	hasError=false;
	constructor(public navCtrl: NavController, 
				public navParams: NavParams,
				public loadingCtrl:LoadingController,
				public alertCtrl:AlertController,
				public authService:AuthServiceProvider) {
	}

	Login()
	{
		loading = this.loadingCtrl.create({
			content: 'Logging in . . .'
		});

		loading.present();
		this.authService.Login(this.email,this.password).then((result)=>{

				this.data= result;
				
				if(this.data)
				{
					loading.dismiss();
					if(this.data.success==true)
					{
						localStorage.clear();
						localStorage.setItem('token', this.data.data.token);
						localStorage.setItem('userData', this.data.data.id);
						this.navCtrl.setRoot(CategoryListPage);
					}
					
				}
				

		},(err)=>{
			
			loading.dismiss();	
			let alert = this.alertCtrl.create({
				title: 'Wrong credentials',
				subTitle: 'Please check your email and password',
				cssClass: 'alertSingleButton',
				buttons: ['ok']
				
			
			});
				alert.present();
			console.log(err);
		});
	}
	GoToRegister()
	{
		this.navCtrl.setRoot(RegisterPage);
	}

}
